import { Config, loadConfigFromEnv } from "$lib/config"
import type { GetSession, Handle } from "@sveltejs/kit"

interface Locals {
	config: Config
}

interface Session {
	config: Config
}

export const handle: Handle<Locals> = async ({ request, resolve }) => {
	loadConfigFromEnv().match(
		(config) => {
			request.locals.config = config
		},
		(error) => {
			throw error
		},
	)

	const response = await resolve(request)

	return response
}

export const getSession: GetSession<Locals, unknown, Session> = async (request) => {
	const { config } = request.locals
	return { config }
}
