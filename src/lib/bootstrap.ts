import { writable } from "svelte/store"
import type { Writable } from "svelte/store"
import { ResultAsync } from "neverthrow"
import type { Config } from "$lib/config"
import CachedRepo from "$lib/infra/in_memory/cached_repo"
import HttpTileJsonRepo from "$lib/infra/tile_server/http_tile_json_repo"
import InMemoryIndicatorRepo from "$lib/infra/in_memory/indicator_repo"
import OmiApiIndicatorRepo from "$lib/infra/omi_api/indicator_repo"
import IndicatorServices from "$lib/model/services/indicator_services"
import type { ErrorResult } from "$lib/model/errors"
import type { IndicatorRepoStore } from "$lib/infra/svelte/stores/types"
import { createRepoStore } from "$lib/infra/svelte/stores/repo_store"
import OmiApiIndicatorDataRepo from "$lib/infra/omi_api/indicator_data_repo"
import InMemoryIndicatorDataRepo from "$lib/infra/in_memory/indicator_data_repo"
import type TileJsonRepo from "$lib/model/repositiories/tile_json_repo"
import type { TileJson } from "$lib/model/entities/tile_json"
import type { Query } from "$lib/infra/url"
import AddressRepo from "$lib/model/AddressRepo"
import ControllerFactory from "$lib/model/controllers/controller_factory"
import type { SplitMapController } from "$lib/model/SplitMapController"

export function initApp(config: Config, query: Query): ResultAsync<Container, ErrorResult> {
	return ResultAsync.fromPromise(
		buildContainer(config, query),
		(error) =>
			({
				type: "InitAppError",
				cause: error,
			} as ErrorResult),
	).andThen((container) => initData(container).map(() => container))
}

export interface Container {
	cachedTileJsonRepo: TileJsonRepo
	config: Config
	controllerFactory: ControllerFactory
	indicatorRepoStore: IndicatorRepoStore
	indicatorServices: IndicatorServices
	inMemoryIndicatorDataRepo: InMemoryIndicatorDataRepo
	inMemoryIndicatorRepo: InMemoryIndicatorRepo
	omiApiIndicatorDataRepo: OmiApiIndicatorDataRepo
	omiApiIndicatorRepo: OmiApiIndicatorRepo
	showIndicatorSelectorModalStore: Writable<boolean>
	splitMapController: SplitMapController
	splitMapControllerStore: Writable<SplitMapController>
}

async function buildContainer(config: Config, query: Query): Promise<Container> {
	// Repositories

	const inMemoryIndicatorRepo = new InMemoryIndicatorRepo()
	const omiApiIndicatorRepo = new OmiApiIndicatorRepo(config.omiApiBaseUrl)

	const inMemoryIndicatorDataRepo = new InMemoryIndicatorDataRepo()
	const omiApiIndicatorDataRepo = new OmiApiIndicatorDataRepo(config.omiApiBaseUrl)

	const tileJsonRepo: TileJsonRepo = new HttpTileJsonRepo(config.tileServerServicesUrl)
	const cachedTileJsonRepo: TileJsonRepo = CachedRepo.create<TileJson>({ otherRepo: tileJsonRepo })

	const addressRepo = new AddressRepo(config.addressApiBaseUrl, config.addressApiPriority)

	// Services

	const indicatorServices = new IndicatorServices(
		omiApiIndicatorRepo,
		inMemoryIndicatorRepo,
		omiApiIndicatorDataRepo,
		inMemoryIndicatorDataRepo,
		addressRepo,
		cachedTileJsonRepo,
		(null as unknown) as SplitMapController, // splitMapController will be injected after the creation of splitMapController
		(null as unknown) as Writable<SplitMapController>, // splitMapControllerStore will be injected after the creation of splitMapController
	)

	// Controllers

	const controllerFactory = new ControllerFactory(indicatorServices, config)
	const splitMapController = await controllerFactory.createSplitMapController(query)

	indicatorServices.injectSplitMapController(splitMapController)

	// Stores

	const indicatorRepoStore: IndicatorRepoStore = createRepoStore(inMemoryIndicatorRepo)

	const showIndicatorSelectorModalStore: Writable<boolean> = writable(false)

	const splitMapControllerStore = writable<SplitMapController>(splitMapController)

	indicatorServices.injectSplitMapControllerStore(splitMapControllerStore)

	// Container

	const container = {
		cachedTileJsonRepo,
		config,
		controllerFactory,
		indicatorRepoStore,
		indicatorServices,
		inMemoryIndicatorDataRepo,
		inMemoryIndicatorRepo,
		omiApiIndicatorDataRepo,
		omiApiIndicatorRepo,
		showIndicatorSelectorModalStore,
		splitMapController,
		splitMapControllerStore,
	}

	return container
}

function initData(container: Container): ResultAsync<void, ErrorResult> {
	const { indicatorServices } = container

	return indicatorServices
		.fetchAndCacheIndicators()
		.map(() => updateSplitMapController(container))
		.mapErr((error) => ({ type: "InitDataError", cause: error }))
}

async function updateSplitMapController(container: Container): Promise<void> {
	const { controllerFactory, splitMapController, splitMapControllerStore } = container
	const mapPanelControllers = await controllerFactory.createMapPanelControllersFromCatalog()
	splitMapController.updateMapPanelControllers(mapPanelControllers)
	splitMapControllerStore.set(splitMapController)
}
