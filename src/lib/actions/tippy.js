import tippy from "tippy.js"

export const defaultParams = {
	interactive: true,
	allowHTML: true,
	theme: "light-border",
	trigger: "click",
	maxWidth: "none",
}

export default function (node, params) {
	const tooltip = tippy(node, { ...defaultParams, ...params })
	return {
    // If the props change, let's update the Tippy instance:
    update: (newParams) => tooltip.setProps({ defaultParams, ...newParams }),

    // Clean up the Tippy instance on unmount:
    destroy: () => tooltip.destroy(),
  };
}
