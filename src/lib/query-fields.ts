import type { Query } from "$lib/infra/url"

function addMapId(field: string, mapId: number) {
	return `${field}${mapId}`
}

export const queryFieldsOfMapPanelControllers = ["s"]
export const queryFieldsOfMapIndicatorsControllers = [
	queryFieldsOfMapPanelControllers.map((item) => addMapId(item, 1)),
	queryFieldsOfMapPanelControllers.map((item) => addMapId(item, 2)),
]
export const indexedQueryFieldsOfControllers = [
	...queryFieldsOfMapIndicatorsControllers[0],
	...queryFieldsOfMapIndicatorsControllers[1],
]
export function sanitizeParsedQuery(parsedQuery: Query): Query {
	const query = { ...parsedQuery }
	let field: string
	for (field of indexedQueryFieldsOfControllers) {
		if (query[field] === undefined || query[field] === null) {
			query[field] = []
		} else {
			if (typeof query[field] === "string") {
				// @ts-expect-error: typeof query[field] === 'string'
				const sanitizedQueryField: string = query[field]
				query[field] = [sanitizedQueryField]
			}
		}
	}
	return query
}
