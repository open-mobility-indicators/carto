import { err, ok, Result } from "neverthrow"
import { z, ZodError } from "zod"
import type { Position } from "$lib/model/value_objects/position"
import { numberArrayToPosition } from "$lib/model/value_objects/position"

function parseFloatCsv(s: string): Position {
	const positionArray = s
		.split(",")
		.map((s) => s.trim())
		.map(parseFloat)
	return numberArrayToPosition(positionArray)
}

function trimEndSlash(s: string): string {
	return s.replace(/\/+$/, "")
}

export interface Config {
	addressApiBaseUrl: string
	addressApiPriority: Position | null
	initialCoordinates: Position
	mapboxAccessToken: string
	omiApiBaseUrl: string
	tileServerServicesUrl: string
}

const positionSchema = z.object({
	lng: z.number(),
	lat: z.number(),
})

const configSchema = z.object({
	addressApiBaseUrl: z.string().url(),
	addressApiPriority: positionSchema.nullable(),
	initialCoordinates: positionSchema,
	mapboxAccessToken: z.string(),
	omiApiBaseUrl: z.string().url(),
	tileServerServicesUrl: z.string().url(),
})

export interface ConfigLoadError {
	cause: ZodError
	type: "ConfigLoadError"
}

export function loadConfigFromEnv(): Result<Config, ConfigLoadError> {
	const omiApiBaseUrl = trimEndSlash(import.meta.env.VITE_OMI_API_BASE_URL)

	const mapboxAccessToken = import.meta.env.VITE_MAPBOX_ACCESS_TOKEN

	const addressApiBaseUrl = trimEndSlash(import.meta.env.VITE_ADDRESS_API_BASE_URL)

	const addressApiPriority = import.meta.env.VITE_ADDRESS_API_PRIORITY_LAT_LON
		? parseFloatCsv(import.meta.env.VITE_ADDRESS_API_PRIORITY_LAT_LON)
		: null

	const tileServerServicesUrl = trimEndSlash(import.meta.env.VITE_TILE_SERVER_SERVICES_URL)

	const initialCoordinates = parseFloatCsv(import.meta.env.VITE_INITIAL_COORDINATES)

	const configData = {
		addressApiBaseUrl,
		addressApiPriority,
		initialCoordinates,
		mapboxAccessToken,
		omiApiBaseUrl,
		tileServerServicesUrl,
	}

	const parseResult = configSchema.safeParse(configData)
	if (!parseResult.success) {
		return err({ type: "ConfigLoadError", cause: parseResult.error })
	}

	return ok(parseResult.data)
}
