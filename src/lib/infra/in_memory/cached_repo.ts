import type { ErrorResult } from "$lib/model/errors"
import { okAsync, ResultAsync } from "neverthrow"

export type Id = string

export default interface CanGet<T> {
	get(id: Id): ResultAsync<T, ErrorResult>
}

export default class CachedRepo<T> implements CanGet<T> {
	constructor(private readonly cache: Map<Id, T>, private readonly otherRepo: CanGet<T>) {}

	static create<T>({ otherRepo }: { otherRepo: CanGet<T> }): CachedRepo<T> {
		const cache = new Map()
		return new CachedRepo(cache, otherRepo)
	}

	get(id: Id): ResultAsync<T, ErrorResult> {
		const entity = this.cache.get(id)
		if (entity !== undefined) {
			return okAsync(entity)
		}

		return this.otherRepo.get(id).map((entity) => {
			this.cache.set(id, entity)
			return entity
		})
	}
}
