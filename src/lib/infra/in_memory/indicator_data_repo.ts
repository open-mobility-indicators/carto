import { err, ok, Result } from "neverthrow"
import type { IndicatorData, IndicatorDataId } from "$lib/model/IndicatorData"
import type { IndicatorDataRepoError } from "$lib/model/repositiories/indicator_data_repo"

export default class InMemoryIndicatorDataRepo {
	constructor(private readonly indicatorDataItems = new Map<IndicatorDataId, IndicatorData>()) {}

	getIndicatorDataById(id: IndicatorDataId): Result<IndicatorData, IndicatorDataRepoError> {
		const indicatorData = this.indicatorDataItems.get(id)
		if (indicatorData === undefined) {
			return err({ type: "IndicatorDataNotFound", id })
		}
		return ok(indicatorData)
	}

	getIndicatorDataItems(): IndicatorData[] {
		return [...this.indicatorDataItems.values()]
	}

	save(indicatorData: IndicatorData): void {
		this.indicatorDataItems.set(indicatorData.id, indicatorData)
	}
}
