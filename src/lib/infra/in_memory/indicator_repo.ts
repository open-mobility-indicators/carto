import { err, ok, Result } from "neverthrow"
import type { Indicator, IndicatorSlug } from "$lib/model/Indicator"
import type { IndicatorRepoError } from "$lib/model/repositiories/indicator_repo"

export default class InMemoryIndicatorRepo {
	constructor(private readonly indicators = new Map<IndicatorSlug, Indicator>()) {}

	getIndicatorBySlug(slug: IndicatorSlug): Result<Indicator, IndicatorRepoError> {
		const indicator = this.indicators.get(slug)
		if (indicator === undefined) {
			return err({ type: "IndicatorNotFound", slug })
		}
		return ok(indicator)
	}

	getIndicators(): Indicator[] {
		return [...this.indicators.values()]
	}

	save(indicator: Indicator): void {
		this.indicators.set(indicator.slug, indicator)
	}
}
