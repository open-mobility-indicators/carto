import { fetchJson } from "$lib/infra/network"
import type { IndicatorDataRepoError } from "$lib/model/repositiories/indicator_data_repo"
import type { IndicatorData, IndicatorDataId } from "$lib/model/IndicatorData"
import { fetchPages } from "$lib/infra/omi_api/pagination"
import type { ResultAsync } from "neverthrow"
import { indicatorDataListResponseToDomain, indicatorDataResponseToDomain } from "$lib/infra/omi_api/mappers"

export default class OmiApiIndicatorDataRepo {
	constructor(private readonly baseUrl: string) {}

	getIndicatorDataById(id: IndicatorDataId): ResultAsync<IndicatorData, IndicatorDataRepoError> {
		const url = `${this.baseUrl}/indicator-data/${id}`
		return fetchJson(url)
			.andThen(indicatorDataResponseToDomain)
			.mapErr((error) => ({ type: "IndicatorDataLoadError", cause: error, id }))
	}

	getIndicatorDataItems(): ResultAsync<IndicatorData[], IndicatorDataRepoError> {
		return fetchPages<IndicatorData>({
			baseUrl: `${this.baseUrl}/indicator-data`,
			parseFunction: indicatorDataListResponseToDomain,
		}).mapErr((error) => ({ type: "IndicatorDataLoadError", cause: error }))
	}
}
