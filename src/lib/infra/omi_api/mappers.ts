import { z, ZodError } from "zod"
import { indicatorDataSchema, indicatorSchema } from "$lib/infra/omi_api/schemas"
import { err, ok, Result } from "neverthrow"
import type { Indicator } from "$lib/model/Indicator"
import type { IndicatorData } from "$lib/model/IndicatorData"
import { paginationPage } from "$lib/infra/parsing"
import type { PaginationPage, ParseError } from "$lib/infra/parsing"

export function indicatorResponseToDomain(data: unknown): Result<Indicator, ParseError> {
	const responseSchema = z.object({ indicator: indicatorSchema })
	let parsedData
	try {
		parsedData = responseSchema.parse(data)
	} catch (error) {
		if (error instanceof ZodError) {
			return err({ type: "ParseError", data, cause: error })
		}
		throw error
	}
	return ok(parsedData.indicator)
}

export function indicatorListResponseToDomain(data: unknown): Result<PaginationPage<Indicator>, ParseError> {
	const responseSchema = z.object({ indicators: paginationPage(indicatorSchema) })
	let parsedData
	try {
		parsedData = responseSchema.parse(data)
	} catch (error) {
		if (error instanceof ZodError) {
			return err({ type: "ParseError", data, cause: error })
		}
		throw error
	}
	return ok(parsedData.indicators)
}

export function indicatorDataResponseToDomain(data: unknown): Result<IndicatorData, ParseError> {
	const responseSchema = z.object({ indicator_data: indicatorDataSchema })
	let parsedData
	try {
		parsedData = responseSchema.parse(data)
	} catch (error) {
		if (error instanceof ZodError) {
			return err({ type: "ParseError", data, cause: error })
		}
		throw error
	}
	return ok(parsedData.indicator_data)
}

export function indicatorDataListResponseToDomain(data: unknown): Result<PaginationPage<IndicatorData>, ParseError> {
	const responseSchema = z.object({ indicator_data: paginationPage(indicatorDataSchema) })
	let parsedData
	try {
		parsedData = responseSchema.parse(data)
	} catch (error) {
		if (error instanceof ZodError) {
			return err({ type: "ParseError", data, cause: error })
		}
		throw error
	}
	return ok(parsedData.indicator_data)
}
