import { okAsync, Result, ResultAsync } from "neverthrow"
import { StringifiableRecord, stringifyUrl } from "query-string"
import { fetchJson } from "$lib/infra/network"
import type { FetchError } from "$lib/infra/network"
import type { PaginationPage, ParseError } from "$lib/infra/parsing"

export function fetchPages<T>({
	baseUrl,
	page = 1,
	parseFunction,
	size = 10,
	queryParams = {},
}: {
	baseUrl: string
	page?: number
	parseFunction: (data: unknown) => Result<PaginationPage<T>, ParseError>
	size?: number
	queryParams?: StringifiableRecord
}): ResultAsync<T[], FetchError | ParseError> {
	let allItems: T[] = []

	function fetchPage(baseUrl: string, page: number): ResultAsync<PaginationPage<T>, FetchError | ParseError> {
		const query = { page, size, ...queryParams }
		const url = stringifyUrl({ url: baseUrl, query })
		return fetchJson(url).andThen(parseFunction)
	}

	function processNextPageIfNeeded({
		page,
		size,
		total,
		items,
	}: PaginationPage<T>): ResultAsync<T[], FetchError | ParseError> {
		allItems = [...allItems, ...items]
		if (page * size < total) {
			return fetchPage(baseUrl, page + 1).andThen(processNextPageIfNeeded)
		}
		return okAsync(allItems)
	}

	return fetchPage(baseUrl, page).andThen(processNextPageIfNeeded)
}
