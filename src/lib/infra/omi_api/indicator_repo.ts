import type { Indicator, IndicatorSlug } from "$lib/model/Indicator"
import { fetchJson } from "$lib/infra/network"
import { indicatorListResponseToDomain, indicatorResponseToDomain } from "$lib/infra/omi_api/mappers"
import type { IndicatorRepoError } from "$lib/model/repositiories/indicator_repo"
import { fetchPages } from "$lib/infra/omi_api/pagination"
import type { ResultAsync } from "neverthrow"
import { stringifyUrl } from "query-string"

export default class OmiApiIndicatorRepo {
	constructor(private readonly baseUrl: string) {}

	getIndicatorBySlug(slug: IndicatorSlug): ResultAsync<Indicator, IndicatorRepoError> {
		const query = { indicator_data_mode: "ids" }
		const url = stringifyUrl({
			url: `${this.baseUrl}/indicator/${slug}`,
			query,
		})
		return fetchJson(url)
			.andThen(indicatorResponseToDomain)
			.mapErr((error) => ({ type: "IndicatorLoadError", cause: error, slug }))
	}

	getIndicators(): ResultAsync<Indicator[], IndicatorRepoError> {
		return fetchPages<Indicator>({
			baseUrl: `${this.baseUrl}/indicator`,
			parseFunction: indicatorListResponseToDomain,
			queryParams: {
				indicator_data_mode: "ids",
				sort: "name:asc",
			},
		}).mapErr((error) => ({ type: "IndicatorLoadError", cause: error }))
	}
}
