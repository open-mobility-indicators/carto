import { z } from "zod"
import { Indicator } from "$lib/model/Indicator"
import { IndicatorData } from "$lib/model/IndicatorData"

export const indicatorSchema = z
	.object({
		slug: z.string(),
		name: z.string(),
		description: z.string(),
		popup_template: z.string().optional(),
		pre_selected: z.boolean().default(false),
		indicator_data: z.array(z.string()).optional().default([]),
		work_in_progress: z.boolean().default(false),
	})
	.transform((data) =>
		Indicator.create({
			slug: data.slug,
			name: data.name,
			description: data.description,
			popupTemplate: data.popup_template ?? null,
			preSelected: data.pre_selected,
			indicatorDataIds: data.indicator_data,
			workInProgress: data.work_in_progress,
		}),
	)

const defaultBranchName = "main"

function recordToMap<T>(record: Record<string, T>): Map<string, T> {
	return new Map(Object.entries(record))
}

export const indicatorDataSchema = z
	.object({
		id: z.string(),
		created_at: z.string(),
		parameter_profile: z.object({
			name: z.string(),
			values: z.record(z.any()).transform(recordToMap),
		}),
		pipeline_id: z.string(),
		pre_selected: z.boolean().default(false),
		slug: z.string(),
		source: z.object({
			branch: z.string().optional(),
			commit: z.string(),
			project_url: z.string().url(),
		}),
		work_in_progress: z.boolean().default(false),
	})
	.transform((data) =>
		IndicatorData.create({
			id: data.id,
			slug: data.slug,
			createdAt: data.created_at,
			parameterProfile: data.parameter_profile,
			pipelineId: data.pipeline_id,
			preSelected: data.pre_selected,
			source: {
				branch: data.source.branch ?? defaultBranchName,
				commit: data.source.commit,
				projectUrl: data.source.project_url,
			},
			workInProgress: data.work_in_progress,
		}),
	)
