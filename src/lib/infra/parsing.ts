import type { ErrorResult } from "$lib/model/errors"
import { z, ZodError } from "zod"

export interface ParseError extends ErrorResult {
	type: "ParseError"
	data: unknown
	cause: ZodError
}

export function paginationPage(itemSchema) {
	return z.object({
		items: z.array(itemSchema),
		page: z.number().positive(),
		size: z.number().positive(),
		total: z.number().nonnegative(),
	})
}

export interface PaginationPage<T> {
	items: T[]
	page: number
	size: number
	total: number
}
