import type { TileJson } from "$lib/model/entities/tile_json"
import { err, ok, Result } from "neverthrow"
import { z, ZodError } from "zod"
import type { ParseError } from "$lib/infra/parsing"

const tileJsonSchema = z.object({
	maxzoom: z.number().nonnegative(),
	minzoom: z.number().nonnegative(),
	name: z.string(),
	tiles: z.array(z.string()),
	vector_layers: z.array(
		z.object({
			id: z.string(),
		}),
	),
})

export function tileJsonResponseToDomain(data: unknown): Result<TileJson, ParseError> {
	let parsedData
	try {
		parsedData = tileJsonSchema.parse(data)
	} catch (error) {
		if (error instanceof ZodError) {
			return err({ type: "ParseError", data, cause: error })
		}
		throw error
	}
	return ok(parsedData)
}
