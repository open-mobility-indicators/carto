import { fetchJson } from "$lib/infra/network"
import type { ResultAsync } from "neverthrow"
import type { TileJson, TileJsonName } from "$lib/model/entities/tile_json"
import type TileJsonRepo from "$lib/model/repositiories/tile_json_repo"
import type { TileJsonLoadError } from "$lib/model/repositiories/tile_json_repo"
import { tileJsonResponseToDomain } from "$lib/infra/tile_server/mappers"

export default class HttpTileJsonRepo implements TileJsonRepo {
	constructor(private readonly baseUrl: string) {}

	get(tileJsonName: TileJsonName): ResultAsync<TileJson, TileJsonLoadError> {
		const url = `${this.baseUrl}/${tileJsonName}`
		return fetchJson(url)
			.andThen(tileJsonResponseToDomain)
			.mapErr((error) => ({ type: "TileJsonLoadError", cause: error, tileJsonName }))
	}
}
