import type { Page } from "@sveltejs/kit"
import queryString from "query-string"
import type { ParsedQuery } from "query-string"
import { goto } from "$app/navigation"
import { parametersAsArray } from "$lib/utils"
import type { IndicatorDataId } from "$lib/model/IndicatorData"
import type { IndicatorSlug } from "$lib/model/Indicator"
import { SplitMapController, checkIsValidMapId } from "$lib/model/SplitMapController"
import type { MapPanelController } from "$lib/model/MapPanelController"

export type Query = ParsedQuery<string | string[] | boolean>

/** Used in URL query string parameters `i` and `s` */
export type URLIndicatorId = IndicatorDataId | IndicatorSlug

export function getAvailableIndicatorsFromPageStore($page: Page): URLIndicatorId[] {
	const { query } = $page
	const i = query.get("i")
	return parametersAsArray(i)
}

export function getShownIndicatorsFromPageStore($page: Page, mapId: number): URLIndicatorId[] {
	checkIsValidMapId(mapId)
	const { query } = $page
	const s = query.get(`s${mapId}`)
	return parametersAsArray(s)
}

function buildQueryString(query: Query): string {
	const rawQueryString = queryString.stringify(query, { arrayFormat: "comma" })
	return `?${rawQueryString}`
}

export function getCurrentQueryString(): string {
	// Force adding `?` to empty query strings to match `buildQueryString` behavior,
	// to be able to compare the current query string to a new one.
	const locationSearch = location.search
	return locationSearch === "" ? "?" : locationSearch
}

/** Return whether a redirection should be done to void infinite loops. */
function shouldRedirect(newQueryString: string): boolean {
	const currentQueryString = getCurrentQueryString()
	return newQueryString !== currentQueryString
}

export function updateUrlFromSplitMapController(
	splitMapController: SplitMapController,
	{ replaceState }: { replaceState: boolean } = { replaceState: true },
): void {
	const indicatorControllerMap1: MapPanelController = splitMapController.mapPanelControllers[0]
	const availableInSelectorIndicatorSlugsMap1 = indicatorControllerMap1.getAvailableInSelectorIndicatorsSlugs()
	const shownIndicatorSlugsMap1 = indicatorControllerMap1.getShownIndicatorSlugs()

	const indicatorControllerMap2: MapPanelController = splitMapController.mapPanelControllers[1]
	const shownIndicatorSlugsMap2 = indicatorControllerMap2.getShownIndicatorSlugs()

	const mapParams1 = splitMapController.mapParams[0]
	const mapParams2 = splitMapController.mapParams[1]

	const sharedMapParams = splitMapController.sharedMapParams

	const query: Query = {
		// indicators that are available in selectors of both maps are stored / updated in map1
		i: availableInSelectorIndicatorSlugsMap1,

		// indicators that are shown on map
		s1: shownIndicatorSlugsMap1,
		s2: shownIndicatorSlugsMap2,

		// true when showing 2 maps
		split: splitMapController.doubleMap,

		// bearing
		b: sharedMapParams.bearing,

		// pitch
		p: sharedMapParams.pitch,

		// zoom
		z: sharedMapParams.zoom,
	}

	// center
	if (mapParams1.center !== null) {
		query.c1 = mapParams1.center.join(",")
	}
	if (mapParams2.center !== null) {
		query.c2 = mapParams2.center.join(",")
	}

	const newQueryString = buildQueryString(query)

	if (shouldRedirect(newQueryString)) {
		goto(newQueryString, { replaceState })
	}
}

export function parseQueryString(url: string): Query {
	return queryString.parse(url, { arrayFormat: "comma", parseBooleans: true })
}

export function getImproveThisMapUrl(lng: number, lat: number, zoom: number): string {
	const improveThisMapPrefix = "https://www.openstreetmap.org/edit#map"
	// Open Street Map use integers for zoom
	const _zoom = Math.round(zoom)

	const getImproveThisMapUrl = `${improveThisMapPrefix}=${_zoom}/${lat}/${lng}`
	return getImproveThisMapUrl
}
