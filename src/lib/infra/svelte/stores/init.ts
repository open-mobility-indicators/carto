import { writable } from "svelte/store"
import type { Writable } from "svelte/store"

export const appInitialized: Writable<boolean> = writable(false)
