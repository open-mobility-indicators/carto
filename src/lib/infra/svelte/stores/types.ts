import type InMemoryIndicatorRepo from "$lib/infra/in_memory/indicator_repo"
import type { Indicator } from "$lib/model/Indicator"
import type { RepoStore } from "$lib/infra/svelte/stores/repo_store"

export type IndicatorRepoStore = RepoStore<Indicator, InMemoryIndicatorRepo>
