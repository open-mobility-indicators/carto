import { getContext } from "svelte"
import type { Container } from "$lib/bootstrap"

export const appContextKey = {}

export interface AppContext {
	getContainer: () => Container
}

export function getContainerFromContext(): Container {
	return getContext<AppContext>(appContextKey).getContainer()
}
