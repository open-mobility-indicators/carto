import axios from "axios"
import type { AxiosError } from "axios"
import { ResultAsync } from "neverthrow"
import type { ErrorResult } from "$lib/model/errors"

export async function fetchJsonPromise(url: string): Promise<unknown> {
	const response = await axios.get(url)
	return response.data
}

export interface FetchError extends ErrorResult {
	type: "FetchError" | "NotFoundError"
	url: string
}

export function fetchJson(url: string): ResultAsync<unknown, FetchError> {
	const promise = fetchJsonPromise(url)
	return ResultAsync.fromPromise(promise, (error) => {
		if ((error as AxiosError<unknown>).response?.status === 404) {
			return { type: "NotFoundError", url, cause: error as Error }
		}
		return { type: "FetchError", url, cause: error as Error }
	})
}
