import marked from "marked"

// Note: do not import Eta from "eta" ES module because it did not work.
// Instead, source it in the DOM <script> tag as described in the docs: https://eta.js.org/docs/learn/install#install-eta

function addProseClass(html) {
	// Wrap with Tailwind Typography "prose" class.
	return `<div class="prose prose-sm">${html}</div>`
}

export function addHeadingLevel3(headingText, content) {
	return `### ${headingText}\n\n${content}`
}

export function formatMarkdown(markdown) {
	return addProseClass(marked(markdown))
}

export function formatMarkdownTemplate(template, data) {
	const etaOptions = {
		// In particular: do not remove \n after a <%= %> interpolation tag, because it is needed by the next Markdown step.
		autoTrim: false,
	}
	const markdown = Eta.render(template, data, etaOptions)
	return formatMarkdown(markdown)
}
