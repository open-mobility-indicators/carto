import type { IndicatorData } from "$lib/model/IndicatorData"

export function ensureArray(value) {
	return Array.isArray(value) ? value : [value]
}

export function isNumber(value) {
	return typeof value === "number" && !isNaN(value)
}

export function union<T>(a: T[], b: T[]): T[] {
	return Array.from(new Set([...a, ...b]))
}

export function parametersAsArray(params: string | null): Array<string> {
	if (params === null) {
		return []
	}
	return params.split(",")
}

export function parseToFloatOrNull(s: string): number | null {
	const parsed = parseFloat(s)
	return isNaN(parsed) ? null : parsed
}

export function antichronologicalSort(indicatorData1: IndicatorData, indicatorData2: IndicatorData): number {
	const date1 = Date.parse(indicatorData1.createdAt)
	const date2 = Date.parse(indicatorData2.createdAt)
	return date2 - date1
}
