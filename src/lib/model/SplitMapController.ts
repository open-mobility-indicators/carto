import cloneDeep from "lodash.clonedeep"
import { AppError } from "$lib/errors"
import type { MapPanelController } from "$lib/model/MapPanelController"
import type { IndicatorSlug } from "$lib/model/Indicator"
import type { IndicatorData, IndicatorDataId } from "$lib/model/IndicatorData"

export type LngLat = [number, number]

export interface MapParams {
	center: LngLat | null
}

export interface SharedMapParams {
	bearing: number | null
	pitch: number | null
	zoom: number | null
}

export const validMapIds = [1, 2]

export class SplitMapController {
	constructor(
		private mapPanelControllers: MapPanelController[],
		public doubleMap: boolean,
		// for center
		public mapParams: MapParams[],
		// for bearing, pitch and zoom
		public sharedMapParams: SharedMapParams,
	) {}

	getMapPanelControllerOfMapId(mapId: number): MapPanelController {
		if (!validMapIds.includes(mapId)) {
			throw new InvalidMapId(mapId)
		}
		return this.mapPanelControllers[mapId - 1]
	}

	// resets the mapPanelController of map2 to use the same values as the mapPanelController of map1
	resetMapPanelController2() {
		const mapPanelController2 = cloneDeep(this.mapPanelControllers[0])
		this.mapPanelControllers[1] = mapPanelController2
	}

	// to set the map params only on one map with the id 'mapId'
	setMapParams(mapParams: MapParams, mapId: number): void {
		const mapParamsCurrentMap = this.mapParams[mapId - 1]
		mapParamsCurrentMap.center = mapParams.center
	}

	// to update the map params on both maps in function of the maps params of the map with the 'mapId' passed in arguments
	updateMapParams(mapParams: MapParams, sharedMapParams: SharedMapParams, mapId: number): void {
		this.setMapParams(mapParams, mapId)
		this.sharedMapParams = sharedMapParams
	}

	// indicators available in selector are always the same for both maps
	setIndicatorsAvailableInSelector(indicatorSlug: IndicatorSlug, availableInSelector: boolean): void {
		const { mapPanelControllers } = this
		for (const mapPanelController of mapPanelControllers) {
			mapPanelController.setIndicatorsAvailableInSelector(indicatorSlug, availableInSelector)
		}
	}

	// indicator data items shown on map are always the same for both maps
	setIndicatorDataShownOnMap(indicatorDataId: IndicatorDataId, shownOnMap: boolean): void {
		const { mapPanelControllers } = this
		for (const mapPanelController of mapPanelControllers) {
			mapPanelController.setIndicatorDataShownOnMap(indicatorDataId, shownOnMap)
		}
	}

	createIndicatorDataControllerIfNotExists(indicatorData: IndicatorData): void {
		// TODO forward to map panel controllers, create missing
		for (const mapPanelController of this.mapPanelControllers) {
			mapPanelController.createIndicatorDataControllerIfNotExists(indicatorData)
		}
	}

	updateMapPanelControllers(mapPanelControllers: MapPanelController[]): void {
		this.mapPanelControllers = mapPanelControllers
	}

	// reset a splitMapController to the lists of available and shown indicators
	setIndicatorsAvailableAndShown(
		availableInSelector: IndicatorSlug[],
		shownOnMap1: IndicatorSlug[],
		shownOnMap2: IndicatorSlug[],
	): void {
		const mapPanelController1 = this.mapPanelControllers[0]
		mapPanelController1.setListOfIndicatorsAvailableInSelector(availableInSelector)
		mapPanelController1.setListOfShownInSelector(shownOnMap1)
		const mapPanelController2 = this.mapPanelControllers[1]
		mapPanelController2.setListOfIndicatorsAvailableInSelector(availableInSelector)
		mapPanelController2.setListOfShownInSelector(shownOnMap2)
	}

	// reset a splitMapController to the lists of available indicators and uncheck all the shown indicators
	setListOfIndicatorsAvailableInSelectorAndResetShownIndicators(availableInSelector: IndicatorSlug[]): void {
		const mapPanelController1 = this.mapPanelControllers[0]
		mapPanelController1.setListOfIndicatorsAvailableInSelector(availableInSelector)
		mapPanelController1.disableAllIndicators()
		const mapPanelController2 = this.mapPanelControllers[1]
		mapPanelController2.setListOfIndicatorsAvailableInSelector(availableInSelector)
		mapPanelController2.disableAllIndicators()
	}
}

export class InvalidMapId extends AppError<{ mapId: number }> {
	constructor(mapId: number) {
		super(`Invalid mapId "${mapId}"`, { mapId })
	}
}

export function checkIsValidMapId(mapId: number): void {
	if (!validMapIds.includes(mapId)) {
		throw new InvalidMapId(mapId)
	}
}
