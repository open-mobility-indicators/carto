import type { IndicatorSlug } from "$lib/model/Indicator"
import type { ErrorResult } from "$lib/model/errors"

export interface IndicatorLoadError {
	type: "IndicatorLoadError"
}

export interface IndicatorNotFound {
	type: "IndicatorNotFound"
	slug: IndicatorSlug
}

export type IndicatorRepoError = ErrorResult & (IndicatorLoadError | IndicatorNotFound)
