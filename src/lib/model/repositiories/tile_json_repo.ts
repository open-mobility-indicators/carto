import type { ResultAsync } from "neverthrow"
import type { TileJson, TileJsonName } from "$lib/model/entities/tile_json"
import type { ErrorResult } from "$lib/model/errors"

export interface TileJsonLoadError extends ErrorResult {
	type: "TileJsonLoadError"
	tileJsonName: TileJsonName
}

export default interface TileJsonRepo {
	get(tileJsonName: TileJsonName): ResultAsync<TileJson, TileJsonLoadError>
}
