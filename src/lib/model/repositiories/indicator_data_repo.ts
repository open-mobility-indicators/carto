import type { ErrorResult } from "$lib/model/errors"
import type { IndicatorDataId } from "$lib/model/IndicatorData"

export interface IndicatorDataLoadError {
	type: "IndicatorDataLoadError"
}

export interface IndicatorDataNotFound {
	type: "IndicatorDataNotFound"
	id: IndicatorDataId
}

export type IndicatorDataRepoError = ErrorResult & (IndicatorDataLoadError | IndicatorDataNotFound)
