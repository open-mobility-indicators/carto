import type { IndicatorDataId } from "$lib/model/IndicatorData"

export type IndicatorSlug = string

export class Indicator {
	constructor(
		public slug: IndicatorSlug,
		public name: string,
		public description: string,
		public popupTemplate: string | null,
		public preSelected: boolean,
		public indicatorDataIds: IndicatorDataId[],
		public workInProgress: boolean,
	) {}

	static create({
		slug,
		name,
		description,
		popupTemplate = null,
		preSelected = false,
		indicatorDataIds = [],
		workInProgress = false,
	}: {
		slug: IndicatorSlug
		name: string
		description: string
		popupTemplate: string | null
		preSelected: boolean
		indicatorDataIds: IndicatorDataId[]
		workInProgress: boolean
	}): Indicator {
		return new Indicator(slug, name, description, popupTemplate, preSelected, indicatorDataIds, workInProgress)
	}
}
