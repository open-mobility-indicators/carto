export interface ErrorResult {
	type: string
	cause?: Error | Error[] | ErrorResult | ErrorResult[]
}
