import type { Indicator, IndicatorSlug } from "$lib/model/Indicator"

export class IndicatorController {
	slug: IndicatorSlug
	availableInSelector: boolean
	shownOnMap: boolean

	constructor(slug: IndicatorSlug, availableInSelector: boolean, shownOnMap: boolean) {
		this.slug = slug
		this.availableInSelector = availableInSelector
		this.shownOnMap = shownOnMap
	}

	static fromIndicator(indicator: Indicator): IndicatorController {
		let availableInSelector = false
		let shownOnMap = false
		if (indicator.preSelected) {
			availableInSelector = true
			shownOnMap = true
		}
		return new IndicatorController(indicator.slug, availableInSelector, shownOnMap)
	}
}
