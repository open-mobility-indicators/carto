export type TileJsonName = string

export interface VectorLayer {
	id: string
}

export interface TileJson {
	maxzoom: number
	minzoom: number
	name: TileJsonName
	tiles: string[]
	vector_layers: VectorLayer[]
}
