import type { Writable } from "svelte/store"
import { combine, Result, ResultAsync } from "neverthrow"
import type InMemoryIndicatorRepo from "$lib/infra/in_memory/indicator_repo"
import type OmiApiIndicatorRepo from "$lib/infra/omi_api/indicator_repo"
import type { Indicator, IndicatorSlug } from "$lib/model/Indicator"
import type { IndicatorData, IndicatorDataId } from "$lib/model/IndicatorData"
import type OmiApiIndicatorDataRepo from "$lib/infra/omi_api/indicator_data_repo"
import type InMemoryIndicatorDataRepo from "$lib/infra/in_memory/indicator_data_repo"
import type { IndicatorRepoError } from "$lib/model/repositiories/indicator_repo"
import type { IndicatorDataRepoError } from "$lib/model/repositiories/indicator_data_repo"
import type AddressRepo from "$lib/model/AddressRepo"
import type { SplitMapController } from "$lib/model/SplitMapController"
import type TileJsonRepo from "$lib/model/repositiories/tile_json_repo"
import type { TileJson } from "$lib/model/entities/tile_json"
import type { TileJsonLoadError } from "$lib/model/repositiories/tile_json_repo"
import { antichronologicalSort } from "$lib/utils"

export default class IndicatorServices {
	constructor(
		private readonly omiApiIndicatorRepo: OmiApiIndicatorRepo,
		private readonly inMemoryIndicatorRepo: InMemoryIndicatorRepo,
		private readonly omiApiIndicatorDataRepo: OmiApiIndicatorDataRepo,
		private readonly inMemoryIndicatorDataRepo: InMemoryIndicatorDataRepo, // TODO remove
		private readonly addressRepo: AddressRepo,
		private readonly cachedTileJsonRepo: TileJsonRepo,
		private splitMapController: SplitMapController,
		private splitMapControllerStore: Writable<SplitMapController>,
	) {}

	injectSplitMapController(splitMapController: SplitMapController): void {
		this.splitMapController = splitMapController
	}

	injectSplitMapControllerStore(splitMapControllerStore: Writable<SplitMapController>): void {
		this.splitMapControllerStore = splitMapControllerStore
	}

	cacheIndicators(indicators: Indicator[]): void {
		const { inMemoryIndicatorRepo } = this
		for (const indicator of indicators) {
			inMemoryIndicatorRepo.save(indicator)
		}
	}

	fetchAndCacheIndicators(): ResultAsync<void, IndicatorRepoError | IndicatorRepoError[]> {
		const { omiApiIndicatorRepo } = this
		return omiApiIndicatorRepo.getIndicators().map((indicators) => {
			this.cacheIndicators(indicators)
		})
	}

	async fetchFeaturesFromAddressApi(keyword: string): Promise<unknown> {
		return this.addressRepo.findFeaturesByKeyword(keyword)
	}

	findIndicatorDataItemsOfIndicator(indicator: Indicator): ResultAsync<IndicatorData[], IndicatorDataRepoError> {
		const results = indicator.indicatorDataIds.map((id) => this.omiApiIndicatorDataRepo.getIndicatorDataById(id))
		return combine(results).map((indicatorDataItems) => {
			indicatorDataItems.sort(antichronologicalSort)
			this.updateIndicatorDataControllers(indicatorDataItems)
			return indicatorDataItems
		})
	}

	updateIndicatorDataControllers(indicatorDataItems: IndicatorData[]): void {
		const { splitMapController, splitMapControllerStore } = this

		for (const indicatorData of indicatorDataItems) {
			splitMapController.createIndicatorDataControllerIfNotExists(indicatorData)
		}

		splitMapControllerStore.set(splitMapController)
	}

	getIndicators(): Indicator[] {
		return this.inMemoryIndicatorRepo.getIndicators()
	}

	getIndicatorBySlug(slug: IndicatorSlug): Result<Indicator, IndicatorRepoError> {
		return this.inMemoryIndicatorRepo.getIndicatorBySlug(slug)
	}

	getIndicatorDataIdsOfIndicatorBySlug(slug: IndicatorSlug): Result<IndicatorDataId[], IndicatorRepoError> {
		return this.getIndicatorBySlug(slug).map((indicator) => indicator.indicatorDataIds)
	}

	getKnownIndicators(): Indicator[] {
		return this.inMemoryIndicatorRepo.getIndicators()
	}

	getKnownIndicatorSlugs(): IndicatorSlug[] {
		const indicators = this.getKnownIndicators()
		return indicators.map((i) => i.slug)
	}

	getKnownIndicatorDataIds(): IndicatorDataId[] {
		const indicators = this.getKnownIndicators()
		return indicators.flatMap((i) => i.indicatorDataIds)
	}

	getShownIndicators(): Indicator[] {
		const indicators = this.getIndicators()
		const { splitMapController } = this
		const mapPanelController = splitMapController.getMapPanelControllerOfMapId(1)
		return mapPanelController.getShownIndicators(indicators)
	}

	getTileJson(indicatorDataId: IndicatorDataId): ResultAsync<TileJson, TileJsonLoadError> {
		return this.cachedTileJsonRepo.get(indicatorDataId)
	}
}
