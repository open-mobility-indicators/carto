import cloneDeep from "lodash.clonedeep"
import type { Config } from "$lib/config"
import type { Query } from "$lib/infra/url"
import { MapPanelController } from "$lib/model/MapPanelController"
import type IndicatorServices from "$lib/model/services/indicator_services"
import { SplitMapController } from "$lib/model/SplitMapController"
import type { LngLat } from "$lib/model/SplitMapController"
import type { MapParams, SharedMapParams } from "$lib/model/SplitMapController"
import { sanitizeParsedQuery } from "$lib/query-fields"
import { parseToFloatOrNull } from "$lib/utils"
import { positionToNumberPair } from "$lib/model/value_objects/position"
import type { IndicatorDataId } from "$lib/model/IndicatorData"
import type { IndicatorSlug } from "$lib/model/Indicator"
import { IndicatorController } from "$lib/model/IndicatorController"
import { IndicatorDataController } from "$lib/model/IndicatorDataController"

export default class ControllerFactory {
	constructor(private readonly indicatorServices: IndicatorServices, private readonly config: Config) {}

	/**
	 * Create a SplitMapController instance using the URL query if it exists.
	 *
	 * If the query is empty, use default values from the catalog.
	 */
	createSplitMapController(query: Query): Promise<SplitMapController> {
		if (Object.keys(query).length === 0) {
			return this.createSplitMapControllerFromConfig()
		} else {
			const sanitizedQuery = sanitizeParsedQuery(query)
			return Promise.resolve(this.createSplitMapControllerFromQuery(sanitizedQuery))
		}
	}

	async createMapPanelControllersFromCatalog(): Promise<MapPanelController[]> {
		const mapPanelController1 = await this.createMapPanelControllerFromCatalog()
		const mapPanelController2 = cloneDeep(mapPanelController1)
		return [mapPanelController1, mapPanelController2]
	}

	private async createSplitMapControllerFromConfig(): Promise<SplitMapController> {
		const { config } = this

		const mapPanelControllers = [this.createMapPanelController(), this.createMapPanelController()]

		const center = null
		const mapParamsMap1: MapParams = { center }
		const mapParamsMap2: MapParams = cloneDeep(mapParamsMap1)
		const mapParams = [mapParamsMap1, mapParamsMap2]

		mapParams[0].center = positionToNumberPair(config.initialCoordinates)
		mapParams[1].center = cloneDeep(mapParams[0].center)

		const sharedMapParams: SharedMapParams = {
			bearing: 0,
			pitch: 0,
			zoom: 13,
		}

		const splitMapController = new SplitMapController(mapPanelControllers, false, mapParams, sharedMapParams)

		return splitMapController
	}

	private createSplitMapControllerFromQuery(query: Query): SplitMapController {
		const doubleMap = Boolean(query.split)
		const indicatorControllerMap1 = this.createMapPanelControllerFromQuery(query, 1)
		const indicatorControllerMap2 = this.createMapPanelControllerFromQuery(query, 2)

		const mapParams = this.buildMapParams(query)
		const sharedMapParams = this.buildSharedMapParams(query)

		return new SplitMapController(
			[indicatorControllerMap1, indicatorControllerMap2],
			doubleMap,
			mapParams,
			sharedMapParams,
		)
	}

	private createMapPanelController({
		indicatorControllers = [],
		indicatorDataControllers = [],
		triggerUpdate = false,
	}: {
		indicatorControllers?: IndicatorController[]
		indicatorDataControllers?: IndicatorDataController[]
		triggerUpdate?: boolean
	} = {}): MapPanelController {
		return new MapPanelController(indicatorControllers, indicatorDataControllers, triggerUpdate, this.indicatorServices)
	}

	private async createMapPanelControllerFromCatalog(): Promise<MapPanelController> {
		const [indicatorControllers, preSelectedIndicatorDataIds] = await this.createIndicatorControllersFromCatalog()
		const indicatorDataControllers = this.createIndicatorDataControllers(preSelectedIndicatorDataIds)
		return this.createMapPanelController({ indicatorControllers, indicatorDataControllers })
	}

	private async createIndicatorControllersFromCatalog(): Promise<[IndicatorController[], IndicatorDataId[]]> {
		const { indicatorServices } = this

		let preSelectedIndicatorDataIds: IndicatorDataId[] = []
		const indicatorControllers: IndicatorController[] = []
		const indicators = indicatorServices.getIndicators()
		for (const indicator of indicators) {
			if (indicator.preSelected) {
				const result = await indicatorServices.findIndicatorDataItemsOfIndicator(indicator)
				if (result.isErr()) {
					throw result
				}
				const indicatorDataItems = result.value
				const preSelectedIds = indicatorDataItems.filter((x) => x.preSelected).map((x) => x.id)
				preSelectedIndicatorDataIds = [...preSelectedIndicatorDataIds, ...preSelectedIds]
			}
			const indicatorController = IndicatorController.fromIndicator(indicator)
			indicatorControllers.push(indicatorController)
		}
		return [indicatorControllers, preSelectedIndicatorDataIds]
	}

	private createMapPanelControllerFromQuery(query: Query, mapId: number): MapPanelController {
		const [indicatorControllers, indicatorDataIdsShownOnMap] = this.createIndicatorControllersFromQuery(query, mapId)
		const indicatorDataControllers = this.createIndicatorDataControllers(indicatorDataIdsShownOnMap)
		return this.createMapPanelController({ indicatorControllers, indicatorDataControllers })
	}

	private createIndicatorControllersFromQuery(query: Query, mapId: number): [IndicatorController[], IndicatorDataId[]] {
		const { indicatorServices } = this

		const indicatorSlugs: IndicatorSlug[] = indicatorServices.getKnownIndicatorSlugs()

		// @ts-expect-error: `i` is always a IndicatorSlug[]
		const slugsOfAvailableInSelector: IndicatorSlug[] = query["i"]

		// @ts-expect-error: `s${mapId}` is always a IndicatorSlug[]
		const slugsOfShownOnMap: IndicatorSlug[] = query[`s${mapId}`]

		const indicatorControllers: IndicatorController[] = []
		let indicatorDataIdsShownOnMap: IndicatorDataId[] = []
		for (const indicatorSlug of indicatorSlugs) {
			const availableInSelector: boolean = slugsOfAvailableInSelector.includes(indicatorSlug)
			// set shownOnMap to true only if the indicator is available in the selector
			const shownOnMap: boolean = slugsOfShownOnMap.includes(indicatorSlug) && availableInSelector

			if (shownOnMap) {
				indicatorServices.getIndicatorBySlug(indicatorSlug).match(
					(indicator) => {
						indicatorDataIdsShownOnMap = indicatorDataIdsShownOnMap.concat(indicator.indicatorDataIds)
					},
					(error) => {
						// Should never happen
						throw error
					},
				)
			}
			const indicatorController = new IndicatorController(indicatorSlug, availableInSelector, shownOnMap)
			indicatorControllers.push(indicatorController)
		}

		return [indicatorControllers, indicatorDataIdsShownOnMap]
	}

	private createIndicatorDataControllers(preSelectedIndicatorDataIds: IndicatorDataId[]): IndicatorDataController[] {
		return preSelectedIndicatorDataIds.map((indicatorDataId) => {
			const shownOnMap = preSelectedIndicatorDataIds.includes(indicatorDataId)
			return new IndicatorDataController(indicatorDataId, shownOnMap)
		})
	}

	private buildMapParams(query: Query): MapParams[] {
		const mapParamsMap1: MapParams = {
			center: parseLngLat(query.c1),
		}
		const mapParamsMap2: MapParams = {
			center: parseLngLat(query.c2),
		}
		return [mapParamsMap1, mapParamsMap2]
	}

	private buildSharedMapParams(query: Query): SharedMapParams {
		const bearing = parseToFloatOrNull(query.b)
		const pitch = parseToFloatOrNull(query.p)
		const zoom = parseToFloatOrNull(query.z)
		return { bearing, pitch, zoom }
	}
}

// TODO should be in infra/url
function parseLngLat(center: [number, number]): LngLat | null {
	if (!Array.isArray(center) || center.length !== 2) {
		return null
	}
	try {
		const [lng, lat] = center.map((item) => parseFloat(item))
		return [lng, lat]
	} catch {
		console.error("Could not parse lngLat from query string, ignoring it")
	}
	return null
}
