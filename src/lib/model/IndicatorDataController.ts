import type { IndicatorDataId } from "$lib/model/IndicatorData"

export class IndicatorDataController {
	constructor(public id: IndicatorDataId, public shownOnMap: boolean) {}
}
