import queryString, { StringifiableRecord } from "query-string"
import { fetchJsonPromise } from "$lib/infra/network"
import type { Position } from "$lib/model/value_objects/position"

export default class AddressRepo {
	constructor(private apiBaseUrl: string, private addressApiPriority: Position | null) {}

	private buildUrl(keyword: string): string {
		const query: StringifiableRecord = {
			q: keyword,
			type: "municipality",
			limit: 15,
		}
		if (this.addressApiPriority !== null) {
			query.lat = this.addressApiPriority.lat
			query.lon = this.addressApiPriority.lng
		}
		return queryString.stringifyUrl({
			url: this.apiBaseUrl,
			query,
		})
	}

	getHomonymns(cities: string[]): string[] {
		const _cities: string[] = []
		const homonyms: string[] = []
		for (const city of cities) {
			if (_cities.includes(city) && !homonyms.includes(city)) {
				homonyms.push(city)
			}
			_cities.push(city)
		}
		return homonyms
	}

	async findFeaturesByKeyword(keyword: string): Promise<unknown> {
		const url = this.buildUrl(keyword)
		const data = await fetchJsonPromise(url)
		let features = data.features
		// sort features alphabeticaly by city name
		features.sort(function (a, b) {
			if (a.properties.city < b.properties.city) {
				return -1
			}
			if (a.properties.city > b.properties.city) {
				return 1
			}
			return 0
		})
		let cities = features.map((feature) => feature.properties.city)
		let homonyms = this.getHomonymns(cities)
		if (homonyms.length > 0) {
			features.map((feature) => {
				if (homonyms.includes(feature.properties.city)) {
					feature.properties.label = `${feature.properties.city} ${feature.properties.postcode}`
				}
			})
		}
		return features
	}
}
