import type { IndicatorSlug } from "$lib/model/Indicator"

export type IndicatorDataId = string

export interface ParameterProfile {
	name: string
	values: Map<string, unknown>
}

export interface IndicatorDataSource {
	branch: string
	commit: string
	projectUrl: string
}

export class IndicatorData {
	constructor(
		public id: IndicatorDataId,
		public slug: IndicatorSlug,
		public createdAt: string, // TODO use a Date, update the parser too
		public parameterProfile: ParameterProfile,
		public pipelineId: string,
		public preSelected: boolean,
		public source: IndicatorDataSource,
		public workInProgress: boolean,
	) {}

	static create({
		id,
		slug,
		createdAt,
		parameterProfile,
		pipelineId,
		preSelected = false,
		source,
		workInProgress = false,
	}: {
		id: IndicatorDataId
		slug: string
		createdAt: string
		parameterProfile: ParameterProfile
		pipelineId: string
		preSelected: boolean
		source: IndicatorDataSource
		workInProgress: boolean
	}): IndicatorData {
		return new IndicatorData(id, slug, createdAt, parameterProfile, pipelineId, preSelected, source, workInProgress)
	}
}
