export interface Position {
	lng: number
	lat: number
}

export function numberArrayToPosition(numbers: number[]): Position {
	return { lng: numbers[0], lat: numbers[1] }
}

export function positionToNumberPair(position: Position): [number, number] {
	return [position.lng, position.lat]
}
