import type { Indicator, IndicatorSlug } from "$lib/model/Indicator"
import type { IndicatorData, IndicatorDataId } from "$lib/model/IndicatorData"
import { AppError } from "$lib/errors"
import { IndicatorDataController } from "$lib/model/IndicatorDataController"
import type { TileJson } from "$lib/model/entities/tile_json"
import type { IndicatorController } from "$lib/model/IndicatorController"
import { combine, combineWithAllErrors, errAsync, ResultAsync } from "neverthrow"
import type { TileJsonLoadError } from "$lib/model/repositiories/tile_json_repo"
import type IndicatorServices from "$lib/model/services/indicator_services"
import type { IndicatorRepoError } from "$lib/model/repositiories/indicator_repo"

export interface MapVectorSourcesAndLayers {
	indicator: Indicator
	vectorSources: VectorSource[]
}

export interface VectorSource {
	tileJson: TileJson
	vectorSourceId: string
}

export class MapPanelController {
	constructor(
		public indicatorControllers: IndicatorController[],
		public indicatorDataControllers: IndicatorDataController[],
		public triggerUpdate: boolean,
		private readonly indicatorServices: IndicatorServices,
	) {}

	private getIndicatorController(slug: IndicatorSlug): IndicatorController {
		const indicatorController = this.indicatorControllers.find((x) => x.slug === slug)
		if (indicatorController === undefined) {
			throw new IndicatorControllerNotFound(slug)
		}
		return indicatorController
	}

	isIndicatorShown(slug: IndicatorSlug): boolean {
		const indicatorController = this.getIndicatorController(slug)
		return indicatorController.shownOnMap
	}

	isIndicatorDataShown(indicatorDataId: IndicatorDataId): boolean {
		const indicatorDataController = this.getIndicatorDataController(indicatorDataId)
		return indicatorDataController.shownOnMap
	}

	private getIndicatorControllerIndex(slug: IndicatorSlug): number {
		const indicatorControllerIndex = this.indicatorControllers.findIndex((x) => x.slug === slug)
		if (indicatorControllerIndex === -1) {
			throw new IndicatorControllerNotFound(slug)
		}
		return indicatorControllerIndex
	}

	private getIndicatorDataController(indicatorDataId: IndicatorDataId): IndicatorDataController {
		const indicatorDataController = this.indicatorDataControllers.find((x) => x.id === indicatorDataId)
		if (indicatorDataController === undefined) {
			throw new IndicatorDataControllerNotFound(indicatorDataId)
		}
		return indicatorDataController
	}

	private setIndicatorDataController(indicatorDataController: IndicatorDataController): void {
		const indicatorDataControllerIndex = this.indicatorDataControllers.findIndex(
			(x) => x.id === indicatorDataController.id,
		)
		if (indicatorDataControllerIndex === -1) {
			this.indicatorDataControllers.push(indicatorDataController)
		} else {
			this.indicatorDataControllers[indicatorDataControllerIndex] = indicatorDataController
		}
	}

	setIndicatorShownOnMap(slug: IndicatorSlug, shownOnMap: boolean): void {
		const indicatorControllerIndex = this.getIndicatorControllerIndex(slug)
		const indicatorController = this.indicatorControllers[indicatorControllerIndex]
		indicatorController.shownOnMap = shownOnMap
		this.indicatorControllers[indicatorControllerIndex] = indicatorController
		// trigger a redraw of the layers of the map of this indicator once
		this.triggerUpdate = true
	}

	setIndicatorDataShownOnMap(indicatorDataId: IndicatorDataId, shownOnMap: boolean): void {
		const indicatorDataController = this.getIndicatorDataController(indicatorDataId)
		indicatorDataController.shownOnMap = shownOnMap
		this.setIndicatorDataController(indicatorDataController)
		// trigger a redraw of the layers of the map of this indicator once
		this.triggerUpdate = true
	}

	getShownIndicatorSlugs(): IndicatorSlug[] {
		return this.indicatorControllers.filter((x) => x.shownOnMap).map((controller) => controller.slug)
	}

	getShownIndicatorDataIds(): IndicatorDataId[] {
		return this.indicatorDataControllers.filter((x) => x.shownOnMap).map((x) => x.id)
	}

	getShownIndicatorDataIdsOfIndicator(indicator: Indicator): IndicatorDataId[] {
		const { indicatorDataIds } = indicator
		const allShownIndicatorDataIds = this.getShownIndicatorDataIds()
		return indicatorDataIds.filter((x) => allShownIndicatorDataIds.includes(x))
	}

	getShownIndicators(indicators: Indicator[]): Indicator[] {
		const shownIndicatorsSlugs: IndicatorSlug[] = this.getShownIndicatorSlugs()
		return indicators.filter((x) => shownIndicatorsSlugs.includes(x.slug))
	}

	getAvailableInSelectorIndicatorsSlugs(): IndicatorSlug[] {
		return this.indicatorControllers.filter((x) => x.availableInSelector).map((x) => x.slug)
	}

	getAvailableInSelectorIndicators(indicators: Indicator[]): Indicator[] {
		const availableInSelectorIndicatorsSlugs: IndicatorSlug[] = this.getAvailableInSelectorIndicatorsSlugs()
		return indicators.filter((x) => availableInSelectorIndicatorsSlugs.includes(x.slug))
	}

	setIndicatorsAvailableInSelector(indicatorSlug: IndicatorSlug, availableInSelector: boolean): void {
		const indicatorControllerIndex = this.getIndicatorControllerIndex(indicatorSlug)
		const indicatorController = this.indicatorControllers[indicatorControllerIndex]
		indicatorController.availableInSelector = availableInSelector
		// show / hide an indicator on map in function of is availability on the selector
		indicatorController.shownOnMap = availableInSelector
		this.indicatorControllers[indicatorControllerIndex] = indicatorController
		// trigger a redraw of the layers of the map of this indicator once
		this.triggerUpdate = true
	}

	setListOfIndicatorsAvailableInSelector(indicatorSlugs: IndicatorSlug[]): void {
		let indicatorControllerIndex = 0
		for (const indicatorController of this.indicatorControllers) {
			this.indicatorControllers[indicatorControllerIndex].availableInSelector = indicatorSlugs.includes(
				indicatorController.slug,
			)
			indicatorControllerIndex += 1
		}
	}

	disableAllIndicators(): void {
		// let indicatorControllerIndex = 0
		for (const indicatorControllerIndex in this.indicatorControllers) {
			this.indicatorControllers[indicatorControllerIndex].shownOnMap = false
			// indicatorControllerIndex += 1
		}
		this.triggerUpdate = true
	}

	setListOfShownInSelector(indicatorSlugs: IndicatorSlug[]): void {
		let indicatorControllerIndex = 0
		for (const indicatorController of this.indicatorControllers) {
			this.indicatorControllers[indicatorControllerIndex].shownOnMap = indicatorSlugs.includes(indicatorController.slug)
			indicatorControllerIndex += 1
		}
		this.triggerUpdate = true
	}

	createIndicatorDataController(indicatorData: IndicatorData): void {
		const shownOnMap = indicatorData.preSelected
		const indicatorDataController = new IndicatorDataController(indicatorData.id, shownOnMap)
		this.setIndicatorDataController(indicatorDataController)
	}

	createIndicatorDataControllerIfNotExists(indicatorData: IndicatorData): void {
		try {
			this.getIndicatorDataController(indicatorData.id)
		} catch (error) {
			if (error instanceof IndicatorDataControllerNotFound) {
				this.createIndicatorDataController(indicatorData)
			} else {
				throw error
			}
		}
	}

	getMapVectorSourcesAndLayers(): ResultAsync<MapVectorSourcesAndLayers[], IndicatorRepoError | TileJsonLoadError> {
		const indicatorSlugs = this.getShownIndicatorSlugs()
		const results = indicatorSlugs.map((slug) => this.getVectorSourcesForIndicator(slug))
		return combine(results)
	}

	private getVectorSourcesForIndicator(
		indicatorSlug: IndicatorSlug,
	): ResultAsync<MapVectorSourcesAndLayers, IndicatorRepoError | TileJsonLoadError> {
		const { indicatorServices } = this
		return indicatorServices.getIndicatorBySlug(indicatorSlug).match(
			(indicator) => {
				const shownDataIds = this.getShownIndicatorDataIdsOfIndicator(indicator)
				const results = shownDataIds.map((indicatorDataId) => this.getVectorSourceForIndicatorData(indicatorDataId))
				return ResultAsync.fromSafePromise<[VectorSource[], TileJsonLoadError[]], IndicatorRepoError>(
					partition(results),
				).map(([vectorSources, errors]) => {
					// TODO
					if (errors.length) {
						console.log("Could not fetch some tile JSON", errors)
					}
					return { indicator, vectorSources }
				})
			},
			(error) => errAsync(error),
		)
	}

	private getVectorSourceForIndicatorData(
		indicatorDataId: IndicatorDataId,
	): ResultAsync<VectorSource, TileJsonLoadError> {
		const { indicatorServices } = this
		return indicatorServices.getTileJson(indicatorDataId).map((tileJson) => {
			return { tileJson, vectorSourceId: indicatorDataId }
		})
	}
}

async function partition<T, E>(results: ResultAsync<T, E>[]): Promise<[T[], E[]]> {
	const ok: T[] = []
	const err: E[] = []
	for (const resultAsync of results) {
		const result = await resultAsync
		if (result.isOk()) {
			ok.push(result.value)
		} else {
			err.push(result.error)
		}
	}
	return [ok, err]
}

export class IndicatorDataControllerNotFound extends AppError<{ indicatorDataId: IndicatorDataId }> {
	constructor(indicatorDataId: IndicatorDataId) {
		super(`Indicator data controller "${indicatorDataId}" was not found`, { indicatorDataId })
	}
}

export class IndicatorControllerNotFound extends AppError<{ slug: IndicatorSlug }> {
	constructor(slug: IndicatorSlug) {
		super(`Indicator controller "${slug}" was not found`, { slug })
	}
}
