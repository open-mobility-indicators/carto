<script lang="ts">
	import { createEventDispatcher, tick } from "svelte"
	import MapboxInspect from "mapbox-gl-inspect"
	import IndicatorSelectorControl from "$lib/components/map_controls/IndicatorSelectorControl.svelte"
	import type { Indicator, IndicatorSlug } from "$lib/model/Indicator"
	import SplitModeControl from "$lib/components/map_controls/SplitModeControl.svelte"
	import AddressAutocompleter from "$lib/components/AddressAutocompleter.svelte"
	import type { ViewParams } from "$lib/components/SplitModeLayout.svelte"
	import type { MapPanelController, MapVectorSourcesAndLayers } from "$lib/model/MapPanelController"
	import { addHeadingLevel3, formatMarkdown, formatMarkdownTemplate } from "$lib/markdown"
	import {
		AttributionControl,
		Layer,
		Map as MapboxMap,
		NavigationControl,
		ScaleControl,
		SvelteControl,
		VectorSource,
	} from "@jailbreak-paris/svelte-mapbox-gl"
	import { getContainerFromContext } from "$lib/infra/svelte/context"
	import { getImproveThisMapUrl } from "$lib/infra/url"

	export let map = null
	export let mapId: number
	export let initialViewParams: ViewParams | null = null
	export let mapPanelController: MapPanelController
	export let showSplitModeControl = false
	export let doubleMapEnabled = false

	const { maplibregl } = window

	const container = getContainerFromContext()
	const { config, indicatorRepoStore, splitMapControllerStore } = container

	const ZOOM_MIN = 1
	const ZOOM_MAX = 22

	let node

	let zoom
	let center

	$: if (zoom || center) {
		updateAttributionControl(node)
	}

	const dispatch = createEventDispatcher()

	async function updateAttributionControl(node) {
		// Hack: overwrite ".mapboxgl-ctrl-attrib" with custom HTML.
		// Later, Mapbox GL AttributionControl will not be able to overwrite it because we removed the "div.mapboxgl-ctrl-attrib-inner".
		await tick()
		const attributionControl = node.querySelector(".mapboxgl-ctrl-attrib")
		if (attributionControl === null) {
			return
		}
		let _zoom: number
		let _lat: number
		let _lng: number
		let improveThisMapUrl = "https://www.openstreetmap.org/edit"
		if (zoom === undefined && center === undefined) {
			if (initialViewParams !== null) {
				_zoom = initialViewParams.zoom
				_lat = initialViewParams.center[1]
				_lng = initialViewParams.center[0]
			}
		} else {
			_zoom = zoom
			_lat = center.lat
			_lng = center.lng
		}
		if (_zoom !== undefined && _lat !== undefined && _lng !== undefined) {
			improveThisMapUrl = getImproveThisMapUrl(_lng, _lat, _zoom)
		}
		attributionControl.innerHTML = `
			<a href="http://www.openstreetmap.org/copyright/"  target="_blank">© OpenStreetMap contributors</a>
			<a href="https://www.mapbox.com/about/maps/" target="_blank">© Mapbox</a>
			<a href="${improveThisMapUrl}" target="_blank" rel="noopener nofollow" style="font-weight: 700; margin-left: 2px;">Improve this map</a>
		`
		// Hack: move attribution control to the last element of the bottom-right group.
		attributionControl.parentElement.appendChild(attributionControl)
	}

	let mapVectorSourcesAndLayers: MapVectorSourcesAndLayers[] = []

	let allMapboxLayerIds: string[] = []

	async function updateMapVectorSourcesAndLayers(mapPanelController: MapPanelController) {
		mapPanelController.getMapVectorSourcesAndLayers().map((newMapVectorSourcesAndLayers) => {
			mapVectorSourcesAndLayers = newMapVectorSourcesAndLayers

			allMapboxLayerIds = mapVectorSourcesAndLayers.flatMap(({ vectorSources }) =>
				vectorSources.flatMap(({ vectorSourceId }) => layerTypes.map((suffix) => `${vectorSourceId}-${suffix}`)),
			)

			// Update indicatorSlugByVectorSourceId map that allow to find back the indicator slug from
			// the vector source ID.
			for (const { indicator, vectorSources } of mapVectorSourcesAndLayers) {
				for (const { vectorSourceId } of vectorSources) {
					indicatorSlugByVectorSourceId.set(vectorSourceId, indicator.slug)
				}
			}

			updateMapboxInspectLayerIds(allMapboxLayerIds)
		})
	}

	updateMapVectorSourcesAndLayers(mapPanelController)

	$: if (mapPanelController.triggerUpdate) {
		updateMapVectorSourcesAndLayers(mapPanelController)
		mapPanelController.triggerUpdate = false
	}

	// See paint properties docs at https://docs.mapbox.com/mapbox-gl-js/style-spec/layers/

	// Define different MapBox styles for the available GeoJSON feature types (Polygon, LineString, Point).
	// For polygons, define two styles, one for the fill and one for the stroke, because we could not achieve to render
	// a filled polygon with a stroke line using one style.
	const layerPropsByType = {
		polygons: {
			type: "fill",
			filter: ["==", "$type", "Polygon"],
			paint: {
				"fill-color": ["string", ["get", "color"], "orange"],
				"fill-opacity": ["interpolate", ["linear"], ["zoom"], 8, 0.25, 11, 0.2, 15, 0.4],
			},
		},
		// layer for polygon outlines (https://github.com/mapbox/mapbox-gl-js/issues/3018)
		polygonOutlines: {
			type: "line",
			filter: ["==", "$type", "Polygon"],
			paint: {
				"line-color": "transparent",
				"line-width": ["interpolate", ["linear"], ["zoom"], 8, 0.5, 9, 1, 13, 2],
				"line-opacity": ["interpolate", ["linear"], ["zoom"], 13, 0.1, 15, 1],
			},
		},
		linestrings: {
			type: "line",
			filter: ["==", "$type", "LineString"],
			paint: {
				"line-color": ["string", ["get", "color"], "blue"],
				"line-opacity": ["interpolate", ["linear"], ["zoom"], 11, 0.1, 13, 0.3, 15, 0.5],
				"line-width": ["interpolate", ["linear"], ["zoom"], 8, 0.5, 11, 0.8, 14, 3],
			},
		},
		points: {
			type: "circle",
			filter: ["==", "$type", "Point"],
			paint: {
				"circle-radius": 6,
				"circle-color": ["string", ["get", "color"], "blue"],
				"circle-opacity": 1,
			},
		},
	}

	const layerTypes = Object.keys(layerPropsByType)

	function handleAutocompleterChange(event) {
		const { coordinates } = event.detail.geometry
		map.jumpTo({
			center: coordinates,
			zoom: 13,
			pitch: 0,
			bearing: 0,
		})
	}

	let initialViewParamsApplied = false
	$: if (map !== null && initialViewParams !== null && !initialViewParamsApplied) {
		map.jumpTo(initialViewParams)
		initialViewParamsApplied = true
	}

	const indicatorSlugByVectorSourceId = new Map<string, IndicatorSlug>()

	function buildDefaultPopupHtml(featureProperties) {
		function formatValue(v: unknown): unknown {
			if (typeof v === "string") {
				return v.toLocaleString()
			}
			return v
		}

		const hiddenFeatureProperties = ["color"]
		return Object.entries(featureProperties)
			.filter(([k, _]) => !hiddenFeatureProperties.includes(k))
			.map(([k, v]) => `- ${k}&nbsp;: ${formatValue(v)}`)
			.join("\n")
	}

	function formatPopupHtml(features): string {
		if (!features || features.length === 0) {
			// Should not happen.
			return "No feature selected"
		}

		// Display popup for first feature only if many are clicked.
		const feature = features[0]
		const defaultPopupHtml = buildDefaultPopupHtml(feature.properties)

		const vectorSourceId = feature.source
		const indicatorSlug = indicatorSlugByVectorSourceId.get(vectorSourceId)
		if (indicatorSlug === undefined) {
			return formatMarkdown(defaultPopupHtml)
		}
		return $indicatorRepoStore.getIndicatorBySlug(indicatorSlug).match(
			(indicator) => formatIndicatorPopupHtml(indicator, feature, defaultPopupHtml),
			(error) => {
				// TODO
				console.error(error)
				return defaultPopupHtml
			},
		)
	}

	function formatIndicatorPopupHtml(indicator: Indicator, feature: unknown, defaultPopupHtml: string): string {
		const { popupTemplate } = indicator
		let popupHtml
		if (popupTemplate) {
			try {
				popupHtml = formatMarkdownTemplate(addHeadingLevel3(indicator.name, popupTemplate), feature.properties)
			} catch (error) {
				console.error("Error formatting popup template, fallback to default popup.", {
					error,
					feature,
					popupTemplate,
				})
				popupHtml = `Erreur de rendu du popup.<br>${defaultPopupHtml}`
			}
		} else {
			popupHtml = formatMarkdown(addHeadingLevel3(indicator.name, defaultPopupHtml))
		}
		return popupHtml
	}

	const mapboxInspectControl = new MapboxInspect({
		queryParameters: {
			// The allMapboxLayerIds variable will evolve when the user adds a OMI layer from the catalog modal,
			// for example. Cf updateMapboxInspectLayerIds function.
			layers: allMapboxLayerIds,
		},
		popup: new maplibregl.Popup(),
		renderPopup: formatPopupHtml,
		showInspectButton: false,
		showMapPopup: true,
		showMapPopupOnHover: false,
		showInspectMapPopupOnHover: false,
	})

	function updateMapboxInspectLayerIds(newLayerIds: string[]) {
		// Hack: update MapboxInspect layer IDs by injecting them in the options of the instance,
		// because its API does not expose a function to update options.
		try {
			mapboxInspectControl.options.queryParameters.layers = newLayerIds
		} catch (e) {
			// Fail silently when mapboxInspectControl is not loaded
		}
	}

	// Set zoom to zoom + 1 and keep the same coordinates
	function handleClickOnZoomIn(event: Event) {
		event.stopPropagation()
		const currentZoom = $splitMapControllerStore.sharedMapParams.zoom
		if (!currentZoom || currentZoom === ZOOM_MAX) {
			return
		}
		let newZoom = currentZoom + 1
		// Set cap value of zoom to ZOOM_MAX
		if (newZoom > ZOOM_MAX) {
			newZoom = ZOOM_MAX
		}

		map.flyTo({
			center: map.getCenter(),
			zoom: newZoom,
			pitch: $splitMapControllerStore.sharedMapParams.pitch,
			bearing: $splitMapControllerStore.sharedMapParams.bearing,
		})
	}

	// Set zoom to zoom - 1 and keep the same coordinates
	function handleClickOnZoomOut(event: Event) {
		event.stopPropagation()
		const currentZoom = $splitMapControllerStore.sharedMapParams.zoom
		if (!currentZoom || currentZoom <= ZOOM_MIN) {
			return
		}
		let newZoom = currentZoom - 1
		// Set cap value of zoom to ZOOM_MIN
		if (newZoom < ZOOM_MIN) {
			newZoom = ZOOM_MIN
		}

		map.flyTo({
			center: map.getCenter(),
			zoom: newZoom,
			pitch: $splitMapControllerStore.sharedMapParams.pitch,
			bearing: $splitMapControllerStore.sharedMapParams.bearing,
		})
	}

	// On click on compass button, reset both pitch and bearing
	function handleClickOnCompass(event: Event) {
		event.stopPropagation()
		const currentZoom = $splitMapControllerStore.sharedMapParams.zoom

		map.flyTo({
			center: map.getCenter(),
			zoom: currentZoom,
			pitch: 0,
			bearing: 0,
		})
	}

	function handleLoad() {
		map.addControl(mapboxInspectControl)

		// Override button "Zoom In" to keep the same coordinates
		const buttonZoomIn = node.querySelector(".mapboxgl-ctrl-zoom-in")
		buttonZoomIn.addEventListener("click", handleClickOnZoomIn, true)

		// Override button "Zoom Out" to keep the same coordinates
		const buttonZoomOut = node.querySelector(".mapboxgl-ctrl-zoom-out")
		buttonZoomOut.addEventListener("click", handleClickOnZoomOut, true)

		// Override button "Compass" to reset both the pitch and the bearing
		const buttonCompass = node.querySelector(".mapboxgl-ctrl-compass")
		buttonCompass.addEventListener("click", handleClickOnCompass, true)

		dispatch("mapLoaded", mapId)
	}
</script>

<div class="h-full w-full" use:updateAttributionControl bind:this={node}>
	<MapboxMap
		accessToken={config.mapboxAccessToken}
		style="mapbox://styles/mapbox/streets-v11"
		attributionControl={false}
		bind:map
		on:moveend
		on:load={handleLoad}
		bind:zoom
		bind:center
	>
		<SvelteControl position="top-left">
			<AddressAutocompleter on:change={handleAutocompleterChange} />
		</SvelteControl>
		<NavigationControl />
		{#if showSplitModeControl}
			<SplitModeControl active={doubleMapEnabled} position="top-right" on:changeSplitMode />
		{/if}
		<ScaleControl />
		<IndicatorSelectorControl {mapPanelController} {mapId} />
		<AttributionControl compact={false} />
		{#each mapVectorSourcesAndLayers as { indicator, vectorSources } (indicator.slug)}
			{#each vectorSources as { tileJson, vectorSourceId } (vectorSourceId)}
				<VectorSource id={vectorSourceId} tiles={tileJson.tiles} minzoom={tileJson.minzoom} maxzoom={tileJson.maxzoom}>
					{#each Object.entries(layerPropsByType) as [layerType, layerProps]}
						<Layer
							{...layerProps}
							id={`${vectorSourceId}-${layerType}`}
							layout={{ visibility: "visible" }}
							sourceLayer={tileJson.vector_layers[0].id}
						/>
					{/each}
				</VectorSource>
			{/each}
		{/each}
	</MapboxMap>
</div>

<style global lang="postcss">
	/* Used by legend tooltips and map feature popups. */
	.legend-square {
		@apply inline-block w-6 h-6 text-center border border-gray-700 rounded;
		background-color: var(--color);
	}

	/* Display the popups of MapboxInspect above the other map controls (e.g. navigation control). */
	.mapboxgl-popup {
		@apply z-50;
	}

	/* Tweak the close button of the MapboxInspect popup */
	.maplibregl-popup-close-button,
	.mapboxgl-popup-close-button {
		font-size: 32px;
		margin-right: 6px;
		margin-top: 6px;
	}
</style>
