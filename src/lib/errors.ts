/*
Hack to fix errors on "npm run build"
used instead of :
import BaseError from "baseerr"
*/
import BaseErrorModule from "baseerr"
const BaseError = BaseErrorModule.default ? BaseErrorModule.default : BaseErrorModule

export class AppError<T> extends BaseError<T> {}
