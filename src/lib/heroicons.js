// From https://heroicons.dev/

export const chevronDown = {
	d: "M19 9l-7 7-7-7",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const chevronUp = {
	d: "M5 15l7-7 7 7",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const externalLink = {
	d: "M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14",
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const locationMarker = {
	d: [
		"M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z",
		"M15 11a3 3 0 11-6 0 3 3 0 016 0z",
	],
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const information = {
	d: ["M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"],
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const eyeOpened = {
	d: ["M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z", "M15 12a3 3 0 11-6 0 3 3 0 016 0z"],
	viewBox: "0 0 24 24",
	variant: "outline",
}

export const eyeClosed = {
	d: ["M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"],
	viewBox: "0 0 24 24",
	variant: "outline",
}
