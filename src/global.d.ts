/// <reference types="@sveltejs/kit" />
/// <reference types="svelte" />
/// <reference types="vite/client" />

interface ImportMetaEnv {
	VITE_ADDRESS_API_BASE_URL: string
	VITE_ADDRESS_API_PRIORITY_LAT_LON?: string
	VITE_INITIAL_COORDINATES?: string
	VITE_MAPBOX_ACCESS_TOKEN: string
	VITE_OMI_API_BASE_URL: string
	VITE_TILE_SERVER_SERVICES_URL: string
}
