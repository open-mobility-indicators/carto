# Carto frontend

This project is a part of the Open Mobility Indicators project (see [archicture](https://gitlab.com/open-mobility-indicators/website/-/wikis/4_Mainteneur/devops#quelle-est-larchitecture-de-la-plateforme-))

`carto` is a web application to display the Open Mobility Indicators on a map.

## Install

```bash
git clone https://gitlab.com/open-mobility-indicators/carto/
cd carto
npm install
cp .env.example .env # adapt values in .env if needed
```

## Development

After the steps from [Install](#Install), you can run the web application with :

```bash
npm run dev
```

Open up [localhost:3000](http://localhost:3000). Server reloads on code change.

## Production

Commits to `master` are automatically deployed to production by Netlify.

Production website : [https://app.openmobilityindicators.org](https://app.openmobilityindicators.org)
